class LicensedsController < ApplicationController
  before_action :set_licensed, only: [:show, :update, :destroy]

  # GET /licenseds
  def index
    @licenseds = Licensed.all

    render json: @licenseds
  end

  # GET /licenseds/1
  def show
    render json: @licensed
  end

  # POST /licenseds
  def create
    @licensed = Licensed.new(licensed_params)

    if @licensed.save
      render json: @licensed, status: :created, location: @licensed
    else
      render json: @licensed.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /licenseds/1
  def update
    if @licensed.update(licensed_params)
      render json: @licensed
    else
      render json: @licensed.errors, status: :unprocessable_entity
    end
  end

  # DELETE /licenseds/1
  def destroy
    @licensed.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_licensed
      @licensed = Licensed.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def licensed_params
      params.require(:licensed).permit(:professor_id, :subject_id)
    end
end
