class Classroom < ApplicationRecord
  belongs_to :professor
  belongs_to :subject
  has_many :subscriptions
  has_many :students, through: :subscriptions
end
