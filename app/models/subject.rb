class Subject < ApplicationRecord
    has_many :linceseds
    has_many :professor, through: :linceseds
    has_many :requeriments
    has_many :disciplines , through: :requeriments, class_name: "Subject"
end
