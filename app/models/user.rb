class User < ApplicationRecord
  enum kind: {
    Admin: 0,
    Aluno: 1,
    Professor: 2
}

has_one :professor
has_one :student

  devise :database_authenticatable,
         :jwt_authenticatable,
         jwt_revocation_strategy: JwtBlacklist
end