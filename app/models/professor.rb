class Professor < ApplicationRecord
  belongs_to :user
  has_many :classrooms
  has_many :licenseds
  has_many :subjects , through: :licenseds
end
