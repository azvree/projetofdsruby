require 'test_helper'

class LicensedsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @licensed = licenseds(:one)
  end

  test "should get index" do
    get licenseds_url, as: :json
    assert_response :success
  end

  test "should create licensed" do
    assert_difference('Licensed.count') do
      post licenseds_url, params: { licensed: { professor_id: @licensed.professor_id, subject_id: @licensed.subject_id } }, as: :json
    end

    assert_response 201
  end

  test "should show licensed" do
    get licensed_url(@licensed), as: :json
    assert_response :success
  end

  test "should update licensed" do
    patch licensed_url(@licensed), params: { licensed: { professor_id: @licensed.professor_id, subject_id: @licensed.subject_id } }, as: :json
    assert_response 200
  end

  test "should destroy licensed" do
    assert_difference('Licensed.count', -1) do
      delete licensed_url(@licensed), as: :json
    end

    assert_response 204
  end
end
