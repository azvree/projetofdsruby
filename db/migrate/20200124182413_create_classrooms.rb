class CreateClassrooms < ActiveRecord::Migration[5.2]
  def change
    create_table :classrooms do |t|
      t.string :initials
      t.boolean :monday
      t.boolean :tuesday
      t.boolean :wednesday
      t.boolean :thursday
      t.boolean :friday
      t.time :time
      t.integer :status
      t.references :professor, foreign_key: true
      t.references :subject, foreign_key: true

      t.timestamps
    end
  end
end
